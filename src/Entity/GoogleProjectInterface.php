<?php

namespace Drupal\gclient\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for entity.
 */
interface GoogleProjectInterface extends ConfigEntityInterface {

}
